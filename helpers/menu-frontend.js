const getMenuFrontEnd = (role = 'USER_ROLE') => {
  const menu = [
    {
      titulo: 'Dashboard',
      icono: 'mdi mdi-gauge',
      subMenu: [
        {titulo: 'Main', url:'/'},
        {titulo: 'Prgressbar', url:'progress'},
        {titulo: 'Gráficas', url:'grafica1'},
        {titulo: 'Promesas', url:'promesas'},
        {titulo: 'Rxjs', url:'rxjs'}
      ]
    },
    {
      titulo: 'Mantenimiento',
      icono: 'mdi mdi-folder-lock-open',
      subMenu: [
        {titulo: 'Hospitales', url:'hospitales'},
        {titulo: 'Medicos', url:'medicos'},
      ]
    }
  ];

  if(role === 'ADMIN_ROLE'){
    //unshift agrega elemento en la primer posición del arreglo
    menu[1].subMenu.unshift({titulo: 'Usuarios', url:'usuarios'});
  }
  return menu;
}

module.exports = {
  getMenuFrontEnd
}