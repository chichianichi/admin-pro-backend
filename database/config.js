/**
 * usuario: manjaro
 * pass: FBiWBfMQ267ufZZW
 * 
 * mongodb+srv://manjaro:<password>@cluster0.4ikdutw.mongodb.net/test
 */
const mongoose = require('mongoose');
require('dotenv').config();

const dbConnection = async () => {
  try {
    await mongoose.connect(process.env.DB_CNN);

    console.log('DB Online');
  } catch (error) {
    console.log(error)
    throw new Error('Error al levantar la base de datos');
  }
}

module.exports = {
  dbConnection
}