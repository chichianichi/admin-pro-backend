/*
  Path: /api/hospitales
*/

const { Router } = require('express');
const { check } = require('express-validator');

const { validarCampos } = require ('../middlewares/validar-campos');
const { validarJWT } = require('../middlewares/validar-jwt');

const {
  getHospitales,
  crearHospital,
  actualizarHospital,
  borrarHospital
} = require('../controllers/hospitales');

const router = Router();

router.get('/', validarJWT, getHospitales);
//el segundo argumento de una ruta es un midleware
router.post(
  '/', 
  [
    validarJWT,
    check('nombre', 'El nombre del hospital es necesario').not().isEmpty(),
    validarCampos
  ], 
  crearHospital);
  

router.put(
  '/:id', 
  [
    validarJWT,
    check('nombre', 'El nombre del hospital debe ser valido').not().isEmpty(),
    //validamos la propiedad que es in id de MongoDB
    check('hospital', 'El hospital id debe ser válido').isMongoId(),
    validarCampos
  ],
  actualizarHospital);  

router.delete('/:id', validarJWT, borrarHospital);  

module.exports = router;