require('dotenv').config();

const path = require('path');

const express = require('express');
var cors = require('cors');

const { dbConnection } = require('./database/config');

//crear servidor express
const app = express();

//configuracion de cors
app.use(cors());

//carpeta pública
app.use(express.static('public'));

//lectura de parseo body
app.use(express.json());

//base de datos
dbConnection();

//rutas
app.use('/api/usuarios', require('./routes/usuarios'));
app.use('/api/hospitales', require('./routes/hospitales'));
app.use('/api/medicos', require('./routes/medicos'));
app.use('/api/todo', require('./routes/busquedas'));
app.use('/api/login', require('./routes/auth'));
app.use('/api/uploads', require('./routes/uploads'));

//En el caso que no encuentre la ruta redirige al archivo
app.get('*', (req, res) => {
  res.sendFile(path.resolve(__dirname, 'public/index.html'));
});


//indicamos el puerto
app.listen(process.env.PORT, () => {
  console.log('Servidor corriendo en el puerto ', process.env.PORT);
})